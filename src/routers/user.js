const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const auth = require('../middleware/auth')
const multer = require('multer')
const {sendWelcomeEmail, sendCancellationEmail} = require('../emails/account')
// router.get('/test', (req, res) => {
//     res.send('from my other router')
// })

router.post('/users', async (req, res) => {
    const user = new User(req.body)
    
    try {
        await user.save()
        sendWelcomeEmail(user.email, user.firstname)
        const token  = await user.generateAuthToken()
        res.status(201).send({user, token})
    } catch (e){
       res.status(400).send(e)
       console.log(e)
    }
   
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email,req.body.password)
        const token = await user.generateAuthToken()
       
        //sol1
        //res.send({user: getPublicProfile(), token})

        //solution2
        res.send({user, token})
    }catch(e){
        res.sendStatus(400)
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send()
    }catch (e) {
        res.sendStatus(500)
    }
})

router.post('/users/logoutAll', auth, async(req, res) => {
    try{
        req.user.tokens = []
        await req.user.save()
        res.sendStatus(200)
    }catch (e) {
        res.sendStatus(500)
    }
})
// router.get('/users', auth, async (req,res) => {

//     try {
//         const users = await User.find({})
//         res.send(users)
//     } catch (e){
//         res.status(404).send(e)
//     }
// })

//authenticating currently loggedinuser to get their profile
router.get('/users/me', auth, async (req, res) => {
    await res.send(req.user)
})


router.patch('/users/me', auth, async(req, res) => {
    const _id = req.user._id
    const allowedUpdates = ["firstname", "email", "password","age"]
    const updates = Object.keys(req.body)
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({error: "Invalid updates!"})
        }
        try{
            const user = await User.findById(_id)
            updates.forEach((update) => 
                user[update] = req.body[update]
            )
            await user.save()
            res.send(user)
        }catch (e) {
            return res.sendStatus(400)
        }
    
    // try{
    //     const user = await User.findById(_id)
    //     updates.forEach((update) => 
    //         user[update] = req.body[update]
    //     )
    //     await user.save()
    //     //const user = await User.findByIdAndUpdate(_id,req.body,{ new:true, runValidators: true})
    //     if(!user){
    //         return res.status(404).send()
    //     }
    //     res.send(user)
    // }catch (e) {
    //     return res.status(404).send(e)
    // }

})

//allowing user to delete profile
router.delete('/users/me', auth, async(req, res) =>{
    // const _id = req.user._id
    try {
        // const user = await User.findByIdAndDelete(_id )
        // if(!user){
        //     return res.sendStatus(404)
        // }

        await req.user.remove()
        sendCancellationEmail(req.user.email, req.user.firstname)
        res.send(req.user)
    }catch (e) {
        return res.sendStatus(500)
    }
})

const uploadFile = multer ({
    dest:'files',
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        // if(!file.originalname.endsWith('.pdf')) {
        //     return cb(new Error('File must be a PDF. Please upload a PDF.'))
        // }
        if(!file.originalname.match(/\.(doc|docx|pdf|pptx)$/)){
            return cb(new Error('File must be in word, pdf or pptx formart.'))
        }
        cb(undefined, true)
    }
})

const uploadAvatar = multer ({
   // dest:'avatar',(to save to fs)
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        // if(!file.originalname.endsWith('.pdf')) {
        //     return cb(new Error('File must be a PDF. Please upload a PDF.'))
        // }
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            return cb(new Error('File must be in jpg,jpeg or png formart.'))
        }
        cb(undefined, true)
    }
})

//router.post('/users/upload', uploadFile.single('avatar'), async (req, res) => {
  
    //     res.send()
    // }, (error, req, res, next) =>{
    //     res.status(400).send({error: error.message})
    // })

router.post('/users/me/avatar', auth, uploadAvatar.single('avatar'), async (req, res) => {
    req.user.avatar = req.file.buffer
    await req.user.save()
    res.send()
}, (error, req, res, next) =>{
    res.status(400).send({error: error.message})
})


router.delete('/users/me/avatar', auth, async (req, res) => {
    req.user.avatar = undefined
    await req.user.save()
    res.send()
}, (error, req, res, next) =>{
    res.status(400).send({error: error.message})
})

//serving profile image
router.get('/users/:id/avatar', async (req,res) => {
    try{
        const user = await User.findById(req.params.id)

        if(!user || !user.avatar ){
            throw new Error()
        }
        //setting response header
        res.set('Content-Type', 'image/jpg')
        res.send(user.avatar)
    }catch (e) {
        res.sendStatus(404)
    }
})

module.exports = router