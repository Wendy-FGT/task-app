const express = require('express')
const router = new express.Router()
const Task = require('../models/task')
const auth = require('../middleware/auth')

router.post('/tasks', auth, async (req, res) => {
    //const task = new Task(req.body)

    const task = new Task({
        ...req.body,
        author: req.user._id
    })

    try {
        await task.save()
        res.send(task)
    }
    catch(error) {
        res.status(400).send(error)
    }
})

//GET /tasks?completed=true
//add support for pagination using limit and skip
//GET /tasks?limit=10&skip=20
//ADD SUPPORT FOR SORT
//GET /tasks?sortBy=createdAt_asc//tasks?sortBy=createdAt_desc(descending -1; ascending 1)
router.get('/tasks', auth, async (req,res) => {
    const match = {}
    const sort = {}

    if(req.query.completed){
        match.completed = req.query.completed === 'true'
    }

    if(req.query.sortBy){
        const parts = req.query.sortBy.split('_')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }
    try{
        // const tasks = await Task.find({author: req.user._id})
        // if(tasks.length === 0){
        //     return res.send('You have no tasks.')
        // }
        await req.user.populate({
            path: 'tasks',
            match,
            options:{
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        })
        res.send(req.user.tasks)
    } catch(e) {
        res.status(500).send()
    }
})

router.get('/tasks/:id', auth, async (req,res) => {
    const _Id = req.params.id

    try {
        //const task = await Task.findById(_id)
        const task = await Task.findOne({_Id, author: req.user._id})
        if(!task){
            return res.status(404).send()
        }
        res.send(task)
    }catch (e) {
        res.status(404).send()
    }
})


router.patch('/tasks/:id', auth, async (req, res) => {
    //const _id = req.params.id
    const allowedUpdates = ["description", "completed"]
    const updates = Object.keys(req.body)
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({error: "Invalid updates!"})
        }

        try{
            //const task = await Task.findById(_id)
            const task = await Task.findOne({_id: req.params.id, author:req.user._id})
            
            if(!task){
                return res.status(404).send()
            }

            updates.forEach((update) => {
                task[update] = req.body[update]
            })
            await task.save()
            res.send(task)
            //const task = await Task.findByIdAndUpdate(_id, req.body, {new: true, runValidators: true})
            //const count  = await Task.countDocuments({completed: false})
        }catch (e) {
            return res.status(400).send(e)
        }
})



router.delete('/tasks/:id', auth, async(req, res) =>{
    const _id = req.params.id

    try {
        //const task = await Task.findByIdAndDelete(_id )
        const task = await Task.findOneAndDelete({_id, author:req.user._id})
        if(!task){
            return res.sendStatus(404)
        }
        res.send(task)
    }catch (e) {
        return res.sendStatus(500)
    }
})

module.exports = router
