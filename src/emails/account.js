const sgMail = require('@sendgrid/mail')
const sendgridAPIKey = process.env.SENDGRID_API_KEY

sgMail.setApiKey(sendgridAPIKey)

const sendWelcomeEmail = (email, firstname) => {
    sgMail.send({
        to: email,
        from: 'solviaonuoha@gmail.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the Task App , ${firstname}. Let me know how you get along with the app.`
    })
}

const sendCancellationEmail = (email, firstname) => {
    sgMail.send({
        to: email,
        from: 'solviaonuoha@gmail.com',
        subject: 'We are sad you are leaving :(',
        text: `Goodbye , ${firstname}.  Is there anything we could have done to make you remain.`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancellationEmail
}