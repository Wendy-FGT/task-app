const mongoose = require('mongoose')
const validator = require('validator')
const chalk = require('chalk')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./task')
const jwtSecret = process.env.JWT_SECRET

const userSchema = new mongoose.Schema (
    {
        firstname:{
            type: String,
            required: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            trim:true,
            minlength:7,
            // lowercase: true,
            validate(value){
                // if(value.toLowerCase().includes('password')){
                //     throw new Error(chalk.red('Password must not contain "password"'))
                // } 
                if(validator.contains(value.toLowerCase(), "password")){
                    throw new Error(chalk.red('Password must not contain "password"'))
                }
            }
       },
        email: {
            type: String,
            unique: true,
            required: true,
            trim: true,
            lowercase: true,
            validate(value){
                if(!validator.isEmail(value)){
                    throw new Error('Email is invalid')
                }
            }
        },
        age: {
            type: Number,
            defualt:0,
            validate(value) {
                if(value < 0){
                    throw new Error('Age must be a positive number.')
                }
            }
        },
        tokens : [{
            token:{
                type: String,
                required:true
            }
        }],
        avatar: {
            type: Buffer
        }
       
    },
    {
        timestamps: true
    }
)

userSchema.virtual ('tasks', {
    ref: 'Task',
    localField:'_id',
    foreignField:'author',
})

//FILTERING PROFILE SENT BACK TO CLIENT
//SOLUTION1
// userSchema.methods.getPublicProfile = function () {
//     const user = this
//     const userObject = user.toObject()

//     delete userObject.password
//     delete userObject.tokens

//     return userObject
// }
//SOLUTION2..remove from profile response
userSchema.methods.toJSON = function () {
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar

    return userObject
}

//relationships


//GENERATE AUTH TOKENS
userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({_id: user._id.toString()}, jwtSecret)
    
    user.tokens  = user.tokens.concat({token})
    await user.save()
    return token
}

//FINDING USER CREDENTIALS 
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({email})
    if(!user){
        throw new Error('Unable to login.')
    }
    const isMatch = await bcrypt.compare(password, user.password)
    if(!isMatch) {
        throw new Error('Unable to login')
    }

    return user
}

//HASH PLAINTEXT PASSWORD BEFORE SAVING
userSchema.pre('save', async function (next){
    const user = this
    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})


//Delete User task when user is removed
userSchema.pre('remove', async function (next) {
    const user = this
    await Task.deleteMany({author: user._id})
    next()
})
const User = mongoose.model('User',userSchema)

module.exports = User