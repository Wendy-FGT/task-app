const request = require('supertest')
const app = require('../src/app')
const Task = require('../src/models/task')
const { userOneId, userOne, userTwo,taskOne,taskThree,userTwoId, setupDatabase } = require('./fixtures/db')

beforeEach(setupDatabase)

test('Should create task for user', async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'From my test'
        })
        .expect(200)
    const task = await Task.findById(response.body._id)
    expect(task).not.toBeNull()
    expect(task.completed).toEqual(false)
})

test('All tasks for user one', async () => {
    const response = await request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
        //const task = await Task.findById(response.body._id)
        expect(response.body.length).toEqual(2)
})

test('Should not fetch user task by id if unauthenticated', async () => {
    const response = await request(app)
        .get(`/tasks/${userOneId}`)
        .send()
        .expect(401)
})

test('Should not fetch other users task by id', async () => {
     await request(app)
    .get(`/task/${taskOne._id}`)
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send()
    .expect(404)
})

test('Should fetch only completed tasks', async () => {
    await request(app)
   .get('/tasks?completed=true')
   .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
   .send()
   .expect(200)
})

test('Should fetch only incomplete tasks', async () => {
    await request(app)
   .get('/tasks?completed=false')
   .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
   .send()
   .expect(200)
})

test('Should not allow unauthorized user delete task not created by them', async () => {
    const response = await request(app)
    .delete(`/task/${taskOne._id}`)
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send()
    .expect(404)

    const task = await Task.findById(taskOne._id)
    expect(task).not.toBeNull()
})

test('Should not delete task if unauthenticated', async () => {
    const response = await request(app)
    .delete(`/task/${taskOne._id}`)
    .send()
    .expect(404)

    const task = await Task.findById(taskOne._id)
    expect(task).not.toBeNull()
})

test('Should not create task with invalid description/completed', async () => {
   await request(app)
    .post('/tasks')
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send({
        description:876433,
        completed:"do again"
    })
    .expect(400)
})

test('Should not update task with invalid description/completed', async () => {
    await request(app)
     .patch(`/tasks/${taskThree._id}`)
     .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
     .send({
         description:"",
         completed:""
     })
     .expect(400)
 })

test('Should not allow unauthorized user update task not created by them', async () => {
    const response = await request(app)
    .patch(`/task/${taskOne._id}`)
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send()
    .expect(404)

    const task = await Task.findById(taskOne._id)
    expect(task).not.toBeNull()
})

