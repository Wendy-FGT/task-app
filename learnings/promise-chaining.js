//WORKING WITH MONGOOSE CHAINING SYNTAX

require('../src/db/mongoose')
const User = require('../src/models/user')

// User.findByIdAndUpdate('6316d32dac512210e381a23b', {age:1}).then((user) => {
//     console.log(user)
//     return User.countDocuments({age:1})
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })

//PROMISE CHAINING CHALLENGE
const updateAgeAndCount = async  (id, age) => {
    const user = await User.findByIdAndUpdate(id, {age})
    const count = await User.countDocuments({age})
    return count
}

updateAgeAndCount('6315b8fd46f9267ab0cd9ec9', 2).then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})