

const { MongoClient, ObjectID} = require('mongodb')

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'task-manager'

const id = new ObjectID()
console.log(id)
console.log(id.getTimestamp())
//connecting to the database
// creating collections and inserting docucments
//     INSERT ONE
    db.collection('users').insertOne({
        name: "Chinwendu",
        age: 22
    }, (error, result) => {
        if(error) {

            return console.log('Unable to insert user')
        }
        console.log(result)
        console.log(result.insertedId)
    })

//      providing object id value
    db.collection('users').insertOne({
        _id: id,
        name: "Olivia",
        age: 22
    }, (error, result) => {
        if(error) {

            return console.log('Unable to insert user')
        }
        console.log(result)
        console.log(result.insertedId)
    })

//     INSERT MANY
    db.collection('users').insertMany([
        {
            name: 'Ekene',
            age: 25
        },
        {
            name: 'Chinny',
            age: 23
        }
    ],
    (error, result) => {
        if(error){
            return console.log('Unable to insert documents')
        }
        console.log(result.insertedIds)

//     CHALLENGE
    db.collection('tasks').insertMany([
        {
            description:"Study 7 hours a day.",
            completed: false
        },
        {
            description:"Take an hour break",
            completed: true
        },
        {
            description:"Pray for 30 minutes",
            completed: false
        }
    ], (error, result) => {
        if(error){
            return console.log('Unable to insert data')
        }
        console.log(result.insertedIds)
    })

    //USING FIND() AND FINDONE()
     db.collection('users').findOne({_id: new ObjectID("63121395fb9507598445deca")}, (error, user) => {
        if(error){
            return console.log('Unable to fetch user.')
        }
        console.log(user)
    })

    //USING CURSOR(COUNT(), TOARRAY()) TO LIMIT/FILTER DATA RECEIVED
    db.collection('users').find({age:22}).toArray((error, users) => {
        if(error){
            return console.log('Unable to connect to database')
        }
        console.log(users)
    })

    //CHALLENGE read/find document
   db.collection('tasks').findOne({_id: new ObjectID("6312155499b5fa8bae9ac958")}, (error, task) => {
    if(error){
        return console.log('Unable to connect to database')
    }
    console.log(task)
   })

   db.collection('tasks').find({completed:false}).toArray((error, tasks) => {
    if(error){
        return console.log('Unable to connect to database')
    }
    console.log(tasks)
   })

      //UPDATE DOC (ONE/MANY)
//    const updatePromise = db.collection('users').updateOne({_id : new ObjectId("631218780dd234e0571dfeb1")}, {
//     $set: {
//         age:21
//     }
//    })

//    updatePromise.then((result) => {
//         console.log(result)
//         }).catch((error) => {
//             console.log(error)
//     })

    //OR

    db.collection('users').updateOne({_id : new ObjectId("631218780dd234e0571dfeb1")}, {
        $set: {
            age:21
        }
       }).then((result) => {
            console.log(result)
            }).catch((error) => {
                console.log(error)
        })

        db.collection('users').updateOne({_id : new ObjectId("63121395fb9507598445deca")}, {
            $inc: {
                age: 5
            }
           }).then((result) => {
                console.log(result)
                }).catch((error) => {
                    console.log(error)
            })
    
   //UPDATE DOC (ONE/MANY)
//    const updatePromise = db.collection('users').updateOne({_id : new ObjectId("631218780dd234e0571dfeb1")}, {
//     $set: {
//         age:21
//     }
//    })

//    updatePromise.then((result) => {
//         console.log(result)
//         }).catch((error) => {
//             console.log(error)
//     })

//UPDATE CHALLENGE
// db.collection('tasks').updateMany({
//     completed: false
// }, {
//     $set:{
//         completed: true
//     }
// }).then((results) => {
//     console.log(results)
// }).catch((error) => {
//     console.log(error)
// })
    
// })

// //CRUD OPERATIONS
// //destructuring

// const { MongoClient, ObjectID, Db, ObjectId} = require('mongodb')

// const connectionURL = 'mongodb://127.0.0.1:27017'
// const databaseName = 'task-manager'


// MongoClient.connect(connectionURL, {useNewUrlParser: true}, (error, client) =>{
//     if(error){
//         return console.log('Unable to connect to database')
//     }
//     //console.log('CONNECTED CORRECTLY')
    
//     const db = client.db(databaseName)

//     //DELETE DOCUMENTS
//     db.collection('users').deleteMany({
//         age:22
//     }).then((results) => {
//         console.log(results)
//     }).catch((error) => {
//         console.log(error)
//     })

//     //CHALLENGE
//     db.collection('tasks').deleteOne({
//         description:"Take an hour break"
//     }).then((results) => {
//         console.log(results)
//     }).catch((error) => {
//         console.log(error)
//     })
// })

        })
