const mongoose = require('mongoose')
const validator = require('validator')
const chalk = require('chalk')


const User = mongoose.model('User',{
    firstname:{
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim:true,
        minlength:7,
        lowercase: true,
        validate(value){
            // if(value.toLowerCase().includes('password')){
            //     throw new Error(chalk.red('Password must not contain "password"'))
            // } 
            if(validator.contains(value.toLowerCase(), "password")){
                throw new Error(chalk.red('Password must not contain "password"'))
            }
        }
   },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    age: {
        type: Number,
        defualt:0,
        validate(value) {
            if(value < 0){
                throw new Error('Age must be a positive number.')
            }
        }
    },
   
})

// const me = new User({
//     firstname: "Chinwendu",
//     email:'CHINWENDY@FLUTTWERAVEG.IO ',
//     password:'PASSWORD123'
// })

// me.save().then((result) => {
//     console.log(me)
// }).catch((error) => {
//     console.log('Error!',error)
// })

module.exports = User