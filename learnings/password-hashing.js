const bcrypt = require('bcryptjs')

const myFunction = async () => {
    const password = 's0lvia123'
    const hashedPassword = await bcrypt.hash(password,8)

    console.log(password)
    console.log(hashedPassword)

    const isMatch = await bcrypt.compare('djskjdks', hashedPassword)
    console.log(isMatch)
}

myFunction()