
require('../src/db/mongoose')
const Task = require('../src/models/task')

// Task.findByIdAndDelete('63170af5707ba86b9e94ae77')
// .then(() => {
//     return Task.countDocuments({completed: false})
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })

const deleteTaskAndFind = async (id) => {
    const task = await Task.findByIdAndDelete(id)
    const countIncompletetasks = await Task.countDocuments({completed: false})
    return countIncompletetasks
}

deleteTaskAndFind('6316d42251bfaada003cd03b').then((task) => {
    console.log(task)
}).catch((e) => {
    console.log(e)
})